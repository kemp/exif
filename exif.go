// Provides support for reading EXIF data from JPEG files.
package exif

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"time"
)

var (
	errUnsupportedFormat      = errors.New("Unsupported format")
	errUnsupportedByteOrder   = errors.New("Unsupported byte order")
	errUnsupportedResultType  = errors.New("Unsupported result type")
	errExpectingTimeFieldType = errors.New("Expecting field type to be time.Time")
)

// Date format string used in EXIF tags.
const exifDate = "2006:01:02 15:04:05"

// Rational represents a rational number expressed as a numerator and a denomenator stored in an
// array with two uint32 components.
type Rational [2]uint32

// Returns a string representation of the rational number.
func (r Rational) String() string {
	return fmt.Sprintf("%d/%d", r[0], r[1])
}

// Orientation represents an EXIF orientation value.
type Orientation uint16

const (
	// The normal orientation for an image (typically portrait).
	Normal Orientation = iota + 1
	// The horizontal mirror of the normal orientation for an image.
	MirrorHorizontal
	// The 180 degree rotation of the normal orientation for an image.
	Rotate180
	// The vertical mirror of the normal orientation for an image.
	MirrorVertical
	// The 270 degree clockwise rotation of the horizontal mirror of the normal orientation for an
	// image.
	MirrorHorizontalRotate270CW
	// The 90 degree clockwise rotation of the normal orientation for an image.
	Rotate90CW
	// The 90 degree clockwise rotation of the horizontal mirror of the normal orientation for an
	// image.
	MirrorHorizontalRotate90CW
	// The 270 degree clockwise rotation of the normal orientation for an image.
	Rotate270CW
)

// CommonTags provides a convenient definition for reading commonly used tags.
type CommonTags struct {
	Manufacturer string      `exif:"0x010F"`
	Model        string      `exif:"0x0110"`
	Timestamp    time.Time   `exif:"0x9003"`
	Orientation  Orientation `exif:"0x0112"`
	WhitePoint   Rational    `exif:"0x013E"`
	ExposureTime Rational    `exif:"0x829A"`
	Aperture     float64     `exif:"0x9202"`
	FocalLength  int         `exif:"0x920A"`
}

// Decode reads JPEG data from the data slice and fills in the result value with decoded data.
//
// Currently, the only supported interface value for result is a pointer to a struct.  The 'exif'
// tag on the struct members gives the tag ID that should be copied into the field.
func Decode(data []byte, result interface{}) error {
	// Check JPEG header
	if !bytes.Equal(data[0:4], []byte{0xFF, 0xD8, 0xFF, 0xE1}) ||
		!bytes.Equal(data[6:10], []byte{'E', 'x', 'i', 'f'}) {
		return errUnsupportedFormat
	}
	data = data[12:]

	// Check TIFF header
	if !bytes.Equal(data[0:2], []byte{'I', 'I'}) {
		return errUnsupportedByteOrder
	}

	tags := make(map[uint16][]byte)

	mainIfd := ifd(data[8:])
	for i := 0; i < fields(mainIfd); i++ {
		id := tagId(mainIfd, i)
		switch id {
		case 0x8769:
			exifIfd := ifd(data[tagValue(mainIfd, i):])
			for j := 0; j < fields(exifIfd); j++ {
				tags[tagId(exifIfd, j)] = tagData(exifIfd, j)
			}
		default:
			tags[id] = tagData(mainIfd, i)
		}
	}

	t := reflect.TypeOf(result)
	if t.Kind() != reflect.Ptr {
		return errUnsupportedResultType
	}
	t = t.Elem()
	if t.Kind() != reflect.Struct {
		return errUnsupportedResultType
	}

	v := reflect.ValueOf(result).Elem()
	for i := 0; i < t.NumField(); i++ {
		exifTag := t.Field(i).Tag.Get("exif")
		if exifTag == "" {
			continue
		}

		id, err := strconv.ParseUint(exifTag, 0, 16)
		if err != nil {
			return err
		}
		if tag, ok := tags[uint16(id)]; ok {
			format := binary.LittleEndian.Uint16(tag[0:2])
			switch format {
			case 2: // ASCII string
				offset := decodeUint32(tag[6:10])
				count := decodeUint16(tag[2:4])
				s := string(data[offset : offset+uint32(count-1)])

				switch t.Field(i).Type.Kind() {
				case reflect.String:
					v.Field(i).SetString(s)
				case reflect.Struct:
					time, err := time.Parse(exifDate, s)
					if err != nil {
						return err
					}
					if v.Field(i).Type() != reflect.TypeOf(time) {
						return errExpectingTimeFieldType
					}
					v.Field(i).Set(reflect.ValueOf(time))
				}

			case 3: // short
				n := decodeUint16(tag[6:8])
				if t.Field(i).Type.Kind() == reflect.Uint16 {
					v.Field(i).SetUint(uint64(n))
				}

			case 5: // rational
				offset := decodeUint32(tag[6:10])
				n, d := decodeUint32(data[offset:offset+4]), decodeUint32(data[offset+4:offset+8])

				switch t.Field(i).Type.Kind() {
				case reflect.Array:
					v.Field(i).Index(0).SetUint(uint64(n))
					v.Field(i).Index(1).SetUint(uint64(d))
				case reflect.Float32, reflect.Float64:
					v.Field(i).SetFloat(float64(n) / float64(d))
				case reflect.Int:
					v.Field(i).SetInt(int64(n) / int64(d))
				}
			}
		}
	}

	return nil
}

func decodeUint32(data []byte) uint32 {
	return binary.LittleEndian.Uint32(data)
}

func decodeUint16(data []byte) uint16 {
	return binary.LittleEndian.Uint16(data)
}

func ifd(data []byte) []byte {
	fields := decodeUint16(data[0:2])
	return data[2 : 2+uint32(fields)*12]
}

func fields(data []byte) int {
	return len(data) / 12
}

func tagId(data []byte, n int) uint16 {
	offset := n * 12
	return decodeUint16(data[offset : offset+2])
}

func tagData(data []byte, n int) []byte {
	return data[n*12+2:]
}

func tagType(data []byte, n int) uint16 {
	offset := n*12 + 2
	return decodeUint16(data[offset : offset+2])
}

func tagValue(data []byte, n int) uint32 {
	offset := n*12 + 8
	return decodeUint32(data[offset : offset+4])
}
